FROM nginx

COPY . /srv/www/static
RUN mv /srv/www/static/deploy/ntn.conf /etc/nginx/conf.d/default.conf
